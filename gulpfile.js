/* jshint node:true */
'use strict';
// generated on 2014-12-14 using generator-gulp-webapp 0.2.0
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var concat = require('gulp-concat');
var copy = require('gulp-copy');
var closureCompiler = require('gulp-closure-compiler');
var flatten = require('gulp-flatten');
var livereload = require('gulp-livereload');

gulp.task('styles', function () {
  return gulp.src('app/css/**/*.scss')
    .pipe($.plumber())
    .pipe($.rubySass({
      style: 'expanded',
      precision: 10
    }))
    //.pipe($.autoprefixer({browsers: ['last 1 version']}))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('jshint', function () {
  return gulp.src('app/scripts/**/*.js')
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'))
    .pipe($.jshint.reporter('fail'));
});

var shaderDirs = ['app/shaders/*.glsl'];
gulp.task('shaders', function() {
  gulp.src(shaderDirs)
    .pipe(gulp.dest('./dist/shaders'))
});

var libDirs = ['app/lib/*.js'];
gulp.task('libs', function() {
  gulp.src(libDirs)
    .pipe(gulp.dest('./dist/lib'))
});

gulp.task('html', ['styles'], function () {
  var assets = $.useref.assets({searchPath: '{.tmp,app}'});

  return gulp.src('app/*.html')
    .pipe(assets)
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.csso()))
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.if('*.html', $.minifyHtml({conditionals: true, loose: true})))
    .pipe(gulp.dest('dist'));
});

gulp.task('images', function () {
  return gulp.src('app/images/**/*')
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', function () {
  return gulp.src(require('main-bower-files')().concat('app/fonts/**/*'))
    .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
    .pipe($.flatten())
    .pipe(gulp.dest('dist/fonts'));
});

gulp.task('extras', function () {
  return gulp.src([
    'app/*.*',
    '!app/*.html',
    'node_modules/apache-server-configs/dist/.htaccess'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'));
});

gulp.task('clean', require('del').bind(null, ['.tmp', 'dist']));

gulp.task('connect', ['styles'], function () {
  var serveStatic = require('serve-static');
  var serveIndex = require('serve-index');
  var app = require('connect')()
    .use(require('connect-livereload')({port: 35729}))
    .use(serveStatic('.tmp'))
    .use(serveStatic('app'))
    // paths to bower_components should be relative to the current file
    // e.g. in app/index.html you should use ../bower_components
    .use('/bower_components', serveStatic('bower_components'))
    .use(serveIndex('app'));

  require('http').createServer(app)
    .listen(9000)
    .on('listening', function () {
      console.log('Started connect web server on http://localhost:9000');
    });
});

gulp.task('serve', ['connect', 'watch'], function () {
  require('opn')('http://localhost:9000');
});

// inject bower components
gulp.task('wiredep', function () {
  var wiredep = require('wiredep').stream;

  gulp.src('app/styles/*.scss')
    .pipe(wiredep())
    .pipe(gulp.dest('app/styles'));

  gulp.src('app/*.html')
    .pipe(wiredep())
    .pipe(gulp.dest('app'));
});

var jsDirs = [
      'app/js/services/services.js', 'app/js/services/*.js',
      'app/js/directives/directives.js', 'app/js/directives/*.js',
      'app/js/components/filterlist/filterlist.js', 'app/js/components/filterlist/**/*.js',
      'app/js/**/*.js'
    ];
gulp.task('js', function() {
  gulp.src(jsDirs)
    .pipe(concat("filtrStackr.js"))
    .pipe(gulp.dest('./dist/'))
    .pipe(closureCompiler({
      compilerPath: 'closure-compiler/compiler.jar',
      fileName: 'filtrStackr.min.js',
      compilerFlags: {
	compilation_level: 'SIMPLE_OPTIMIZATIONS'
      }
    }))
    .pipe(gulp.dest('./dist/'))
});

var cssDirs = ['app/css/*.css'];
gulp.task('css', function() {
  gulp.src(cssDirs)
    .pipe(gulp.dest('./dist/css'))
});

var partialDirs = ['app/partials/*.html', 'app/js/**/*.html'];
gulp.task('partials', function() {
  gulp.src(partialDirs)
    .pipe(flatten())
    .pipe(gulp.dest('./dist/partials'))
});

gulp.task('watch', ['connect'], function () {
  $.livereload.listen();

  // watch for changes
  gulp.watch([
    'app/*.html',
    '.tmp/styles/**/*.css',
    'app/scripts/**/*.js',
    'app/images/**/*'
  ]).on('change', $.livereload.changed);

  gulp.watch('app/styles/**/*.scss', ['styles']);
  gulp.watch('bower.json', ['wiredep']);
});

gulp.task('build', ['js', 'libs', 'styles', 'css', 'partials', 'shaders', 'html', 'images', 'fonts', 'extras'], function () {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', ['clean'], function () {
  gulp.start('build');
});

//var gulp = require('gulp');
//var $ = require('gulp-load-plugins')();
//var concat = require('gulp-concat');
//var copy = require('gulp-copy');
//var closureCompiler = require('gulp-closure-compiler');
//var flatten = require('gulp-flatten');
//var livereload = require('gulp-livereload');
//var partialDirs = ['app/partials/*.html', 'app/js/**/*.html'];
//gulp.task('partials', function() {
//  gulp.src(partialDirs)
//    .pipe(flatten())
//    .pipe(gulp.dest('./dist/partials'))
//});
//
//var cssDirs = ['app/css/*.css'];
//gulp.task('css', function() {
//  gulp.src(cssDirs)
//    .pipe(gulp.dest('./dist/css'))
//});
//
//var shaderDirs = ['app/shaders/*.glsl'];
//gulp.task('shaders', function() {
//  gulp.src(shaderDirs)
//    .pipe(gulp.dest('./dist/shaders'))
//});
//
//var libDirs = ['app/lib/*.js'];
//gulp.task('libs', function() {
//  gulp.src(libDirs)
//    .pipe(gulp.dest('./dist/lib'))
//});
//
//var indexDir = ['app/index.html'];
//gulp.task('index', function() {
//  gulp.src(indexDir)
//    .pipe(gulp.dest('./dist'))
//});
//
//var jsDirs = [
//      'app/js/services/services.js', 'app/js/services/*.js',
//      'app/js/directives/directives.js', 'app/js/directives/*.js',
//      'app/js/components/filterlist/filterlist.js', 'app/js/components/filterlist/**/*.js',
//      'app/js/**/*.js'
//    ];
//gulp.task('js', function() {
//  gulp.src(jsDirs)
//    .pipe(concat("filtrStackr.js"))
//    .pipe(gulp.dest('./dist/'))
//    .pipe(closureCompiler({
//      compilerPath: 'closure-compiler/compiler.jar',
//      fileName: 'filtrStackr.min.js',
//      compilerFlags: {
//	compilation_level: 'SIMPLE_OPTIMIZATIONS'
//      }
//    }))
//    .pipe(gulp.dest('./dist/'))
//});
//
//gulp.task('watch', function() {
//  livereload.listen();
//  gulp.watch('dist/**').on('change', livereload.changed);
//});
//
//gulp.task('default', ['watch'], function() {
//  gulp.watch(partialDirs, ['partials']);
//  gulp.watch(cssDirs, ['css']); 
//  gulp.watch(shaderDirs, ['shaders']); 
//  gulp.watch(libDirs, ['lib']); 
//  gulp.watch(indexDir, ['index']); 
//  gulp.watch(jsDirs, ['js']);
//});
//
//gulp.task('build', ['partials', 'css', 'shaders', 'libs', 'index'], function () {
//  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
//});
