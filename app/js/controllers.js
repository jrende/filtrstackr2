/* Controllers */

angular.module('filtrstackr.controllers', []).
controller('main', ['$scope', 'Types', 'Filters', 'FilterItemVisibility', 'Renderer', function(sc, types, filters, vis, renderer) {
  var urlParameters = window.location.search.split("[?&]").slice(0);
  sc.isDebug = (urlParameters[0] === "debug=1");
  sc.filters = filters.list;
  sc.types = types.list;
  sc.newFilter = function() {
    console.log("addfilter");
    var filter = {};
    filter.type = types.getTypeById('undefined');
    filters.add(filter);
    _.each(vis, function(elm, i, li) {
      vis[i] = false;
    });
    vis[filter.id] = true;
  }
  sc.$watch('filters', function() {
    renderer.drawScene(filters.list);
  }, true);
}]);
