/* Services */
angular.module('filtrstackr.services')
.factory('Defaults', ["Types", function(types) {
  var list = {
    "gauss": {

    },
    "clouds": {
      "seed": parseInt(Math.random() * 100000)
    },
    "flat": {

    },
    "noise": {

    },
    "blur": {

    },
    "undefined": {

    },
    "example": {
      "invert": false,
      "distortion": 10,
      "smoothness": 1,
      "scaling": 100
    }
    }
    return {
      //Should probably work.
      getDefaults: function(filter){
	if(filter !== undefined) {
	  _.each(filter.type.parameters, function(par, i, li) {
	    if(list[filter.type.id] === undefined || list[filter.type.id][par] === undefined) {
	      //What if it isn't a number!?
	      filter[par] = 1.0;
	    } else {
	      filter[par] = list[filter.type.id][par];
	    }
	  });
	  filter.alpha = 1.0;
	  filter.blendMode = types.getBlendModeById('normal');
	}
      }
    }
}]);

