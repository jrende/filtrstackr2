/* Services */
angular.module('filtrstackr.services').
factory('Filters', ["Types", function(types) {
  var currentIndex = 0;
  return {
    "list" : [ ],
    "add": function(item) {
      item.id = currentIndex++;
      this.list.push(item);
    },
    "remove": function(item){
      _.each(this.list, function(element, index, list) {
	if(element.id === item.id) {
	  list.splice(index, 1);
	}
      });
    },
    "setNewType": function(filter) {
    }
  }
}]);

