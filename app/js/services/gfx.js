angular.module('filtrstackr.services')
.factory('FilterItemVisibility', ["Filters", function(filters) {
  var visibility = [];
  return visibility;
}])
.factory('Renderer', ["Types", function(types) {
  var gl;
  var initCanvas = function() {
    var canvas = document.getElementById("glCanvas"); 
    try {
      gl = canvas.getContext("experimental-webgl");
      gl.viewportWidth = canvas.width;
      gl.viewportHeight = canvas.height;
    } catch (e) {

    }
    if (!gl) {
      alert("Could not initialise WebGL, sorry :-(");
    }
  };
  var squareVertexPositionBuffer;
  var initBuffers = function() {
    squareVertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, squareVertexPositionBuffer);
    var vertices = [
  1.0,  1.0, 0.0,
    -1.0,  1.0, 0.0,
    1.0, -1.0, 0.0,
    -1.0, -1.0, 0.0
      ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    squareVertexPositionBuffer.itemSize = 3;
    squareVertexPositionBuffer.numItems = 4;
  }
  var shaderPrograms = {};
  var createShaders = function(gl) {
    var shaderList = {};
    var req = new XMLHttpRequest();
    req.onload = function(e) {
      if(this.status === 200) {
	var shader = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(shader, this.responseText);
	gl.compileShader(shader);
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
	  console.group(elm.id)
	    console.log(gl.getShaderInfoLog(shader))
	    console.groupEnd(elm.id)
	    return null;
	}
	shaderList["VertexShader"] = shader;
      }
    };
    req.open("GET", "shaders/VertexShader.glsl", false);
    req.setRequestHeader("Accept", "text/plain");
    req.send(null);

    _.each(types.list, function(elm, i, li) {
      var req = new XMLHttpRequest();
      req.onload = function(e) {
	if(this.status === 200) {
	  var shader = gl.createShader(gl.FRAGMENT_SHADER);
	  gl.shaderSource(shader, this.responseText);
	  gl.compileShader(shader);
	  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
	    console.group(elm.id)
	    console.log(gl.getShaderInfoLog(shader))
	    console.groupEnd(elm.id)
	    return null;
	  }
	  shaderList[elm.id] = shader;
	}
      };
      req.open("GET", "shaders/" + elm.id + ".glsl", false);
      req.setRequestHeader("Accept", "text/plain");
      req.send(null);
    });

    _.each(types.list, function(filterType, i, li) {
      var fragmentShader = shaderList[filterType.id]
      var vertexShader = shaderList["VertexShader"];
      if(fragmentShader === undefined || vertexShader === undefined) {
	return;
      }

      var shaderProgram = gl.createProgram();
      gl.attachShader(shaderProgram, vertexShader);
      gl.attachShader(shaderProgram, fragmentShader);
      gl.linkProgram(shaderProgram);

      if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
	alert("Could not initialise shaders");
      }

      gl.useProgram(shaderProgram);

      shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
      gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

      shaderProgram.uniforms = {};
      for(var i = 0; i < filterType.parameters.length; i++) {
	shaderProgram.uniforms[filterType.parameters[i]] = gl.getUniformLocation(shaderProgram, filterType.parameters[i]);
      }
      shaderProgram.uniforms.tSampler = gl.getUniformLocation(shaderProgram, "tSampler");
      shaderProgram.uniforms.res = gl.getUniformLocation(shaderProgram, "res");

      gl.bindBuffer(gl.ARRAY_BUFFER, squareVertexPositionBuffer);
      gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, squareVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

      shaderPrograms[filterType.id] = shaderProgram;
    });
  };
  var texFBO;
  var texture;
  var initTexture = function() {
    texFBO = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, texFBO);
    texFBO.width = 512;
    texFBO.height = 512;

    texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, texFBO.width, texFBO.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.generateMipmap(gl.TEXTURE_2D);


    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);

    gl.bindTexture(gl.TEXTURE_2D, null);
    gl.bindRenderbuffer(gl.RENDERBUFFER, null);
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
  }
  var ready = false;	
  return {
    init: function() {
      initCanvas();
      initBuffers();
      initTexture();
      createShaders(gl);
      gl.clearColor(0.0, 0.0, 0.0, 1.0);
      gl.disable(gl.DEPTH_TEST);
      ready = true;
    },
      drawScene: function(filters) {
	if(ready) {
	  console.log("Drawing scene!");
	  gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
	  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT | gl.STENCIL_BUFFER_BIT);
	  gl.disable(gl.BLEND);
	  _.each(filters, function(filter, i, li) {
	    var shader = shaderPrograms[filter.type.id];
	    if(shader != null) {
	      gl.useProgram(shader);
	      gl.uniform2fv(shader.uniforms["res"], [gl.viewportWidth, gl.viewportHeight]);
	      for(var j = 0; j < filter.type.parameters.length; j++) {
		var parameter = filter.type.parameters[j];
		var argument = filter[parameter];
		if(parameter !== undefined && argument !== undefined) {
		  gl.uniform1f(shader.uniforms[parameter], argument);
		}
	      }
	      if(filter.blendMode !== undefined) {
		//Rita till skärmen, med det förra filtrstackrts textur
		gl.blendFunc(gl[filter.blendMode.srcFactor], gl[filter.blendMode.dstFactor]);
		gl.drawArrays(gl.TRIANGLE_STRIP, 0, squareVertexPositionBuffer.numItems);
		gl.enable(gl.BLEND);
	      }
	    }
	  });
	}
      }
  };
}]);
