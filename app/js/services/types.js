angular.module('filtrstackr.services')
  .factory('Types', function() {
    return {
      "list": [
	{
	  "name": "Clouds",
	  "id": "clouds",
	  "parameters" : [
	    "alpha",
	    "blendMode",
	    "seed",
	    "size",
	    "depth",
	    "nabla",
	    "r",
	    "g",
	    "b"
	  ]
	},
	{
	  "name": "Flat color",
	  "id": "flat",
	  "parameters" : [
	    "alpha",
	    "blendMode",
	    "r",
	    "g",
	    "b"
	  ]
	},
	{
	  "name": "Noise",
	  "id": "noise",
	  "parameters" : [
	    "alpha",
	    "blendMode",
	    "amount"
	  ]
	},
	{
	  "name": "Plaid",
	  "id": "lines",
	  "parameters" : [
	    "alpha",
	    "blendMode",
	    "scale",
	    "r", "g", "b",
	    "x", "y"
	  ]
	},
	{
	  "name": "Undefined filter",
	  "id": "undefined",
	  "parameters" : []
	},
	{
	  "name": "Example",
	  "id": "example",
	  "parameters" : [
	    "alpha",
	    "blendMode",
	    "distortion",
	    "smoothness",
	    "scaling",
	    "invert"
	  ]
	}
      ], 
      "blendModes": [
	{
	  "name": "Normal",
	  "id": "normal",
	  "srcFactor": "SRC_ALPHA",
	  "dstFactor": "ONE_MINUS_SRC_ALPHA"

	},
	{
	  "name": "Multiply",
	  "id": "multiply",
	  "srcFactor": "DST_COLOR",
	  "dstFactor": "ONE_MINUS_SRC_ALPHA"
	},
	{
	  "name": "Screen",
	  "id": "screen",
	  "srcFactor": "SRC_ALPHA",
	  "dstFactor": "ONE_MINUS_SRC_COLOR"
	}
      ],
      "getTypeById": function(id) {
	for(var i = 0; i < this.list.length; i++) {
	  if(this.list[i].id === id) {
	    return this.list[i];
	  }
	}
	return null;
      },
      "getBlendModeById": function(id) {
	for(var i = 0; i < this.blendModes.length; i++) {
	  if(this.blendModes[i].id === id) {
	    return this.blendModes[i];
	  }
	}
	return null;
      },
      "setDefaults": function(filter) {
      },
      "getDefaults": function(filter) {
      }
    }
});
