angular.module('filtrstackr.filterlist.directives', [])
.directive('fsFilterControl', ['Types', function(types) {
  return {
    restrict: "A",
    replace: false,
    transclude: false,
    scope: {
      fsFilterControl: "="
    },
    link: function(scope, iElement, iAttrs, controller) {
      scope.url = "partials/" + scope.fsFilterControl.type.id + ".html";
      scope.types = types.list;
      scope.blendModes = types.blendModes;
    },
    templateUrl: 'partials/filterControl.html'
  }
}])
.directive('sortable', ["Filters", function(filters) {
    return {
      restrict: 'EA',
      transclude: false,
      replace: true,
      scope: {
	fsModel: "="
      },
      compile: function compile(tElement, tAttrs, transclude) {
	return {
	  pre: function preLink(scope, elm, iAttrs, controller) {
	    var list = elm.find("ul.orderlist").sortable({axis: "y", handle: "header"});
	    list.on("sortdeactivate", function() {
	      var next = _.map(this.children, function(elm) {
		return Number(elm.getAttribute('fs-id'));
	      });
	      var newList = [];
	      for(var i = 0; i < next.length; i++) {
		newList[i] = filters.list[next[i]];
	      }
	      scope.$apply(function() {
		angular.copy(newList, filters.list);
	      });
	    });
	    var list = scope[iAttrs.fsModel];
	  },
	  post: function postLink(scope, iElement, iAttrs, controller) {
	  }
	}
      },
      templateUrl: 'partials/filterlist.html'
    }
}])
.directive('filteritem', ["FilterItemVisibility", function(vis) {
  return {
    restrict: "EA",
    replace: false,
    transclude: false,
    scope: {
      fsItem: "=",
      fsIndex: "="
    },
    templateUrl: 'partials/filteritem.html'
  }
}])
.directive('fsSelect', ["Types", "Defaults", function(types, defaults) {
  return {
    restrict: "EA",
    replace: false,
    transclude: false,
    scope: {
      fsSelect: "=",
      fsSelType: "="
    },
    link: function(scope, iElement, iAttrs, controller) {
      iElement.change(function() {
	scope.$apply(function() {
	  var t = iAttrs.fsSelType;
	  if(t === 'type') {
	    var newType = types.getTypeById(iElement.val());
	    if(newType.id != scope.fsSelect.id) {
	      scope.fsSelect.type = newType;
	      defaults.getDefaults(scope.fsSelect);
	    }
	  } else if(t === 'blend') {
	    scope.fsSelect = types.getBlendModeById(iElement.val());
	  }
	});
      });
    }
  }
}])
