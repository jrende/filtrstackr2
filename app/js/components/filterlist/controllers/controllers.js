angular.module('filtrstackr.filterlist.controllers', []).
controller('filterItem', ['$scope', 'FilterItemVisibility', 'Filters', function(sc, vis, filters) {
  sc.visibility = vis;
  sc.getVis = function(item) {
    return vis[item.id];
  }
  sc.toggle = function(item) {
    vis[item.id] = !vis[item.id];
  }
  sc.removeFilter = function(item) {
    console.log("tjo");
    filters.remove(item);
  }
}]);
