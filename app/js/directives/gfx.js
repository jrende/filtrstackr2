angular.module('filtrstackr.directives')
.directive('scene', ["Renderer", function(renderer) {
  return {
    restrict: "EA",
    replace: true,
    transclude: false,
    template: "<canvas id='glCanvas' width='800px' height='800px'></canvas>",
    compile: function compile(tElement, tAttrs, transclude) {
      return {
	pre: function preLink(scope, elm, iAttrs, controller) {
	},
	post: function postLink(scope, iElement, iAttrs, controller) {
		renderer.init();
	}
      }
    }
  }
}]);
