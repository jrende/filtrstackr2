angular.module('filtrstackr.directives')
.directive('input', ['Types', function(types) {
	return {
		restrict: "E",
		replace: false,
		transclude: false,
		require: 'ngModel',
		link: function(scope, iElement, iAttrs, controller) {
			if(iAttrs["type"] === "range") {
				controller.$parsers.push(function (input) {
					return parseFloat(input);
				});
			}
		}
	}
}])
//Changes ngModel twice, unneccesarily.
.directive('seed', [function() {
	return {
		restrict: "E",
		replace: true,
		transclude: false,
		require: 'ngModel',
		template: 
			'<fieldset class="seed">' +
				'<button class="seed-prev">&lt;</button>' +
				'<input type="text" ng-model="ngModel"></input>' +
				'<button class="seed-next">&gt;</button>' +
			'</fieldset>',
		scope: {
			ngModel: "="
		},
		link: function(scope, iElement, iAttrs, controller) {
			var generatedSeeds = [];
			var i = 0;
			iElement.find(".seed-prev").click(function(e) {
				i--;
				if(generatedSeeds[i] === undefined) {
					generatedSeeds[i] = parseInt(Math.random() * 100000);
				}
				scope.$apply(function() {
					scope.ngModel = generatedSeeds[i];
				});
			});
			iElement.find(".seed-next").click(function(e) {
				i++;
				if(generatedSeeds[i] === undefined) {
					generatedSeeds[i] = parseInt(Math.random() * 100000);
				}
				scope.$apply(function() {
					scope.ngModel = generatedSeeds[i];
				});
			});
			scope.$watch('ngModel', function() {
				generatedSeeds[i] = scope.ngModel;
			});
		}
	}
}])
.directive('toRgb', [function() {
  return {
    restrict: "A",
    replace: false,
    transclude: false,
    scope: {
	    toRgb: "=",
	    ngModel: "="
    },
    link: function(scope, iElement, iAttrs, controller) {
      scope.$watch("ngModel", function() {
	if(scope.ngModel !== undefined) {
	  var c = scope.ngModel.substring(1);
	  scope.toRgb.r = parseInt(c.substr(0,2), 16)/255.0;
	  scope.toRgb.g = parseInt(c.substr(2,2), 16)/255.0;
	  scope.toRgb.b = parseInt(c.substr(4,2), 16)/255.0;
	  window.test = iElement;
	}
      });
    }
  }
}]);
