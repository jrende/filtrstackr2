// Declare app level module which depends on filters, and services
angular.module('filtrstackr', [
  'ngRoute',
  'filtrstackr.filters',
  'filtrstackr.services',
  'filtrstackr.directives',
  'filtrstackr.filterlist',
  'filtrstackr.filterlist.directives',
  'filtrstackr.filterlist.controllers',
  'colorpicker.module',
  'filtrstackr.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {controller: 'main'});
  $routeProvider.otherwise({redirectTo: '/view1'});
}]);
